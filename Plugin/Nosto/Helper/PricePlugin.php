<?php
/**
 * @copyright Visma Digital Commerce AS 2019
 * @license Proprietary
 * @author Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\FasterNosto\Plugin\Nosto\Helper;

/**
 * Wraps the PriceHelper from Nosto to use indexers instead of some very
 * expensive operations that caused long load times
 */
class PricePlugin
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $session;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;
    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    private $configurableType;

    /**
     * PricePlugin constructor.
     *
     * @param \Magento\Customer\Model\Session $session
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableType
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(
        \Magento\Customer\Model\Session $session,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableType,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->session = $session;
        $this->resourceConnection = $resourceConnection;
        $this->configurableType = $configurableType;
    }

    /**
     * @param \Nosto\Tagging\Helper\Price $subject
     * @param callable $proceed
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Store\Model\Store $store
     * @param bool $inclTax
     * @param bool $finalPrice
     * @return float
     */
    public function aroundGetProductPrice(
        \Nosto\Tagging\Helper\Price $subject,
        callable $proceed,
        \Magento\Catalog\Model\Product $product,
        \Magento\Store\Model\Store $store,
        $inclTax = true,
        $finalPrice = false
    ) {
        if (!($product->getTypeInstance() instanceof \Magento\ConfigurableProduct\Model\Product\Type\Configurable)) {
            return $proceed($product, $store, $inclTax, $finalPrice);
        }

        $childPrices = $this->getChildPrices($product, $store, $this->session);

        $finalPrices = array_map(function (array $data): float {
            return (float) $data["final_price"];
        }, $childPrices);

        $prices = array_map(function (array $data): float {
            return (float) $data["price"];
        }, $childPrices);

        if ($finalPrice) {
            return $this->getLowestOrZero($finalPrices);
        } else {
            return $this->getLowestOrZero($prices);
        }
    }

    /**
     * @param float[] $prices
     * @return float
     */
    protected function getLowestOrZero(array $prices): float
    {
        $prices = array_filter($prices, function (float $price): bool {
            return $price > 0;
        });

        if (count($prices) > 0) {
            return min($prices);
        }

        return 0.0;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Store\Model\Store $store
     * @param \Magento\Customer\Model\Session $session
     * @return array[]
     */
    protected function getChildPrices(
        \Magento\Catalog\Model\Product $product,
        \Magento\Store\Model\Store $store,
        \Magento\Customer\Model\Session $session
    ): array {
        $select = $this->resourceConnection->getConnection()->select()
            ->from(["i" => $this->resourceConnection->getTableName("catalog_product_index_price")])
            ->joinInner(
                ["r" => $this->resourceConnection->getTableName("catalog_product_super_link")],
                "i.entity_id=r.product_id"
            )
            ->where("i.website_id = :wid")
            ->where("i.customer_group_id = :cgid")
            ->where("r.parent_id = :eid");

        return $this->resourceConnection->getConnection()->fetchAll($select, [
            "wid" => $store->getWebsiteId(),
            "cgid" => $session->getCustomerGroupId(),
            "eid" => $product->getEntityId()
        ]);
    }
}
